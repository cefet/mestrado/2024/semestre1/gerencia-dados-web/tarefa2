Centro Federal de Educação Tecnológica de Minas Gerais (CEFET-MG)

Programa de Pós-Graduação em Modelagem Matemática e Computacional (PPGMMC)

Gerência de Dados da Web

Professores: Thiago Magela Rodrigues Dias e Gray Farias Moita

Estudante: André Almeida Rocha

---

# Tarefa 02 – Extração Simples de Dados
![Web Scraper](images/webscraper_logo.svg "Web Scraper logo")

Para a extração simples de dados foi utilizado a extensão [Web Scraper](https://webscraper.io/), e selecionados os seguintes conjuntos de dados (páginas Web) do Instituto Nacional de Meteorologia:

- [Risco de Incêndio](https://portal.inmet.gov.br/paginas/incendio)
- [Catálogo de Estações Automáticas](https://portal.inmet.gov.br/paginas/catalogoaut)
- [Tabela de Dados das Estações](https://tempo.inmet.gov.br/TabelaEstacoes/A001) para todas as estações automáticas.

![INMET - mapa de risco de incêndio](inmet_risco-incendio.png "INMET - mapa de risco de incêndio")

## Estratégias para coleta dos conjuntos selecionados

### Instalação da extensão no navegador
![Firefox](images/firefox-logo.svg "Firefox browser") ou ![Chrome](images/chrome-logo.svg "Chrome browser")

Para instalar a extensão no navegador siga a documentação da [instalação](https://webscraper.io/documentation/installation).

### Criação das estratégias
![Sitemap](images/sitemap.svg "Sitemap")

Para criar estratégias é necessário as seguintes etapas:

1. abrir a extenção Web Scraper, conforme as documentações abaixo:
- [Documentação - abrir a extensão Web Scraper](https://webscraper.io/documentation/open-web-scraper)
- [Como fazer - abrir a extensão Web Scraper pela primeira vez](https://www.webscraper.io/how-to-video/open-web-scraper)

2. criar um mapeamento dos sites, conforme as documentações abaixo:
- [Documentação - raspar um site](https://webscraper.io/documentation/scraping-a-site)
- [Como fazer - criar um mapeamento de site](https://www.webscraper.io/how-to-video/create-a-sitemap)

Os **mapeamentos dos sites** foram exportados e disponíveis em JSON, e podem ser importados no Web Scraper:
- [Valores de riscos de incêndio para cada estação](mapping/sitemap_risco.json);
- [Legenda de risco de incêndio](mapping/sitemap_legenda.json);
- [Cores de risco de incêndio para cada estação](mapping/sitemap_cores.json);
- [Descrição do Índice de Inflamabilidade de Nesterov (Grau de Perigo)](mapping/sitemap_descricao.json);
- [Catálogo de estações automáticas com metadados](mapping/sitemap_estacoes_metadados.json);
- [Tabela de dados climáticos das estações automáticas](mapping/sitemap_estacoes_dados.json).

## Recuperação de amostra de cada conjunto
![Export data](images/download.svg "Export data")

Para recuperar os dados é necessário realizar a raspagem para cada mapeamento e após a conclusão deve-se exportar esses dados em XLSX ou CSV.

## Resumo completo
Disponível em PDF: [Resumo](AndreRocha_Tarefa_02_Extracao-WebScraper.pdf).
